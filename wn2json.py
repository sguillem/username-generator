import json

try:
    from nltk.corpus import wordnet as wn
except:
	import nltk    
	nltk.download('wordnet')
	from nltk.corpus import wordnet as wn

words = {}
word_types = [wn.NOUN, wn.VERB, wn.ADJ, wn.ADV]
for wt in word_types:
    print("Processing " + str(wt))
    tmp_words = []
    for word in wn.all_synsets(wt):
        illegal_chars = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "-", "_", ":", ".", ",", " "]
        elligible_variants = word.lemma_names()
        for ev in elligible_variants:
            if not any(x in ev for x in illegal_chars):
                tmp_words.append(ev)
    words[wt] = tmp_words

with open("words.json", "w") as out:
    out.write(json.dumps(words))
