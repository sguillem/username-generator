import json
from random import choice, randint

word_types = ['n', 'v', 'a', 'r']
min_username_length = 15
max_username_length = 30
num_substitutions = 5

def loadDataFiles():
    with open("assets/words.json", "r") as wordjson:
        words = json.loads(wordjson.read().strip())
    with open("assets/substitutions.json", "r") as substitutionsjson:
        substitutions = json.loads(substitutionsjson.read().strip())
    return words, substitutions

def randomSubstitute(word, number_of_chars):
    word_indexes = {i: word[i] for i in range(len(word))}
    eligible_word_indexes = {index: letter for index, letter in word_indexes.items() if letter in substitutions.keys()}
    for i in range(min(number_of_chars, len(eligible_word_indexes))):
        next_index = choice(list(eligible_word_indexes.keys()))
        word_indexes[next_index] = substitutions[word_indexes[next_index]]
        del eligible_word_indexes[next_index]
    new_word = ""
    for i in range(len(word)):
        new_word += word_indexes[i]
    return new_word

def getRandomWord():
    wt = choice(word_types)
    return choice(words[wt])

if __name__ == '__main__':
    print("Loading data files...")
    words, substitutions = loadDataFiles()
    print("Creating new random username...")
    username = ""

    while len(username) < min_username_length:
        rnd_word = getRandomWord()
        if len(username) + len(rnd_word) < max_username_length:
            username += rnd_word
        else:
            pass

    username = randomSubstitute(username, num_substitutions)
    print("Your new username: " + username)